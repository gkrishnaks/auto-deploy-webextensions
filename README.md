Auto deploy extensions to Chrome Store and Mozilla Firefox addons gallery.
======================================================================

Web-extension specification lets us write (nearly) same program that can run as addon/extension in both Firefox and Chrome. (and other browsers that may follow WebExtension specification [here](https://browserext.github.io/browserext/))

Building and deploying to Chrome store and Mozilla addons gallery involves several steps when done manually.

This is my take on CICD auto-deploy from GitLab's CICD pipeline to Chrome Store and Mozilla addons gallery in one go :

Values needed For Chrome Store auto-deployment
----------------------------------------------
* Follow this guide to generate CLIENT_ID, CLIENT_SECRET and REFRESH_TOKEN - [Generate API Keys](https://github.com/DrewML/chrome-webstore-upload/blob/master/How%20to%20generate%20Google%20API%20keys.md)
* EXTENSION_ID can be taken from the URL of the your extension in Chrome store. 

Values needed for Mozilla Firefox addons Gallery auto-deployment
--------------------------------------------------------
* Make an initial deployment (manual) into Mozilla addons gallery.
* Generate JWT tokens from here - [Generate tokens](https://addons.mozilla.org/en-GB/developers/addon/api/key/)

Files in this repository :
--------------------------
Take the .deploytoChromeStore.js,  .deploytoAMO.js, .firefoxdeploy.js, .gitlab_ci.yml,  package.json and .user-error.js files from this repository and move them to your Gitlab project.

GitLab CI CD settings / Other CI CD pipeline settings :
-------------------------------------------------
* Open your extension project -> Side pane -> Settings -> CI /CD -> Variables (for Gitlab projects). You need to set similar Environment variables in other CI CD pipelines.
* And set all the tokens generated above for Chrome Store and Firefox addons gallery as variables - Mark protected. Refer the names to be used for each token below.
* If your Project is public in gitlab - In the same page above, toggle OFF your pipeline from Public Pipeline (this is due to security consideration - public pipelines allows anyone to see the logs in case of a public project)

Tokens need to be set in CI CD Environment variables :
--------------------------------------------------
Mozilla Firefox JWT Issuer and secrets need to be set as "values" against these variable names :
```
WEXT_SHIPIT_FIREFOX_JWT_ISSUER 
WEXT_SHIPIT_FIREFOX_JWT_SECRET 
```

Google API tokens need to be set as "values" against these variable names :
```
CLIENT_ID
CLIENT_SECRET
EXTENSION_ID
REFRESH_TOKEN
```

How to trigger an auto-deploy:
------------------------------
* Remember to bump the version number in your extension's manifest.json (semver). It should be bigger than the one listed already in Chrome store/AMO gallery.
* Any commit or merge to MASTER branch will trigger a auto-deploy to both Chrome webstore and Mozilla Firefox Addons gallery.
* Or Go to pipeline and start your pipeline manually.

Congratulations :)

* * *

Credits and Acknowledgements:
-----------------------------
1) For Chrome Store auto-deployment: [Dev.to Article by @gokatz](https://dev.to/gokatz/automate-your-chrome-extension-deployment-in-minutes-48gb)<br/>
(compare the gist snippet in this article against my .deploytoChromeStore.js and .gitlab_ci.yml to understand the changes I made)
   
2) For Firefox Deployment : [wext-shipit npm module](https://github.com/LinusU/wext-shipit)<br/>
(I just needed the firefox deploy script from this npm module. Refer my .gitlab_ci.yml and .deploytoAMO.js, .firefoxdeploy.js files to see the difference)

License:
--------
[CC-BY-SA 4.0](https://gitlab.com/gkrishnaks/auto-deploy-webextensions/blob/master/LICENSE)